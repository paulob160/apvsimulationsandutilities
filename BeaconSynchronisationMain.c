/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* BeaconSynchronisation.c                                                    */
/* 20.09.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <time.h>
#include "BeaconSynchronisation.h"
#include "apv_NRF24L01State.h"
#include "mt19937.h"

/******************************************************************************/
/* Local Variable Declarations :                                              */
/******************************************************************************/

simulationParameter_t           apv_nRF24L01BeaconTimingPointSimulation,
                                slaveReceptionTimingPointSimulation;
                                
uint16_t                        slaveReceptionTimingPoint     = 0;

boolean_t                       signalFound                   = FALSE;
                                                              
double                          randomTiming                  = 0.0;                                                    // use to get random values

int                             testCounter                   = 0;

APV_ERROR_CODE                  synchCode                     = APV_ERROR_CODE_NONE;

/******************************************************************************/
/* Local Function Declarations :                                              */
/******************************************************************************/

int main(void);

/******************************************************************************/
/* Local Function Definitions :                                               */
/******************************************************************************/

int main(void)
  {
/******************************************************************************/

  __time32_t theCurrentTime = 0;

  int        keyPressed     = 0;
  boolean_t  beaconExit     = FALSE;

/******************************************************************************/

  system("cls");

  printf("\n Master-Slave Beacon Synchronisation Simulation : ");
  printf("\n ---------------------------------------------- ");

  printf("\n");

  // Initialise the random-number generator
  init_genrand(_time32(&theCurrentTime));

  // Choose a starting point for the slave synchronisation attempt
  slaveReceptionTimingPointSimulation.simulationParameter     = &slaveReceptionTimingPoint;
  slaveReceptionTimingPointSimulation.simulationParameterType =  SIMULATION_PARAMETER_TYPE_UINT16;
  strcat(slaveReceptionTimingPointSimulation.simulationParameterName, SLAVE_RECEPTION_TIMING_POINT);

  randomTiming              = genrand_real1();
  slaveReceptionTimingPoint = (uint16_t)(randomTiming * ((double)(APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD - 1)));

  printSimulationParameter(&slaveReceptionTimingPointSimulation,
                            TRUE);

  // Choose a starting point for the master beacon transmission - THIS IS THEP RIMARY 
  // TIMING POINT! To the master this is t == 0!
  apv_nRF24L01BeaconTimingPointSimulation.simulationParameter     = &apv_nRF24L01BeaconTimingPoint;
  apv_nRF24L01BeaconTimingPointSimulation.simulationParameterType = SIMULATION_PARAMETER_TYPE_UINT16;
  strcat(apv_nRF24L01BeaconTimingPointSimulation.simulationParameterName, MASTER_TRANSMISSION_TIMING_POINT);

  while (beaconExit != TRUE)
    {
    printf("[%04d] : ", testCounter++);

    synchCode =  apv_nRF24L01BeaconDetection(&signalFound,
                                             &apv_nRF24L01BeaconTimingPoint,
                                              FALSE);

    if (signalFound == TRUE)
      {
      printf("\n Master beacon detected");
      printSimulationParameter(&apv_nRF24L01BeaconTimingPointSimulation,
                               TRUE);
      }
    else
      {
      printf("\n Master beacon NOT detected");
      }

    printf("\n Press <b> to exit this test loop...\n");

    while (!_kbhit())
      ;

    if ((keyPressed = getch()) == BEACON_EXIT)
      {
      beaconExit = TRUE;
      }
    }

  /******************************************************************************/
  /* Now try to pull the two units in to the same timing point. The master      */
  /* timing point is not going to move much so the slave must try to pull in.   */
  /* The simulation drives the slave to opens it's receive window in a series   */
  /* of binary chops, starting with a full period to detect ANY beacon. Here    */
  /* we assume at least some signal has been found...                           */
  /******************************************************************************/

  while (TRUE)
    {
    // At every iteration make a decision on the presence or absence of the beacon
    // BEACON PRESENCE LOCKED ON 21.09.18
    synchCode = apv_nRF24L01BeaconDetection(&signalFound,
                                            &apv_nRF24L01BeaconTimingPoint,
                                             TRUE);

    apv_nRF24L01MasterSlaveBeaconSynchronisation(apv_nRF24L01BeaconFullPeriodStart,
                                                 apv_nRF24L01BeaconFullPeriodEnd,
                                                 apv_nRF24L01InitialiseSearch,
                                                 &apv_nRF24L01BeaconFound,
                                                 &apv_nRF24L01BeaconTiming);

    if (_kbhit())
      {
      break;
      }
    }


  /******************************************************************************/

  printf("\n");

  while (!_kbhit())
    ;

/******************************************************************************/

  return(0);

/******************************************************************************/
  } /* end of main                                                            */

/******************************************************************************/
/* printSimulationParameter() :                                               */
/******************************************************************************/

void printSimulationParameter(simulationParameter_t *simulationParameter,
                              boolean_t              printSimulationParameterNewLine)
  {
/******************************************************************************/

  if (simulationParameter != NULL)
    {
    if (simulationParameter->simulationParameterType < SIMULATION_PARAMETER_TYPES)
      {
      if ((simulationParameter->simulationParameter != NULL) && (strlen(simulationParameter->simulationParameterName) != 0))
        {
        printf("\n %s = ", simulationParameter->simulationParameterName);

        switch(simulationParameter->simulationParameterType)
	        {
          case SIMULATION_PARAMETER_TYPE_UINT8      : 
          case SIMULATION_PARAMETER_TYPE_UCHAR      : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_UINT16     : 
          case SIMULATION_PARAMETER_TYPE_USHORT     : printf("%u", *(uint16_t *)(simulationParameter->simulationParameter));
                                                      break;
          case SIMULATION_PARAMETER_TYPE_UINT32     : 
          case SIMULATION_PARAMETER_TYPE_UINT       : 
          case SIMULATION_PARAMETER_TYPE_ULONG      : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_UINT64     : 
          case SIMULATION_PARAMETER_TYPE_ULONGLONG  : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_INT8       : 
          case SIMULATION_PARAMETER_TYPE_CHAR       : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_INT16      : 
          case SIMULATION_PARAMETER_TYPE_SHORT      : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_INT32      : 
          case SIMULATION_PARAMETER_TYPE_INT        : 
          case SIMULATION_PARAMETER_TYPE_LONG       : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_INT64      : 
          case SIMULATION_PARAMETER_TYPE_LONGLONG   : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_FLOAT      : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_DOUBLE     : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_LONGDOUBLE : 
                                                      break;
          case SIMULATION_PARAMETER_TYPE_STRING     : 
                                                      break;

          default                                   :
                                                      break;
	        }
	      }
      }
    }

  if (printSimulationParameterNewLine == TRUE)
    {
    printf("\n");
    }

/******************************************************************************/
  } /* end of printSimulationParameter                                        */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
