/******************************************************************************/
/*                                                                            */
/* NumericalWrapping.c                                                        */
/* 21.09.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <conio.h>

/******************************************************************************/
/* Constant Definitions :                                                     */
/******************************************************************************/

#define NUMERICAL_WRAPPING_ENDPOINT ((uint16_t)100)

/******************************************************************************/
/* Local Function Declarations :                                              */
/******************************************************************************/

int main(void);

/******************************************************************************/
/* Local Function Definitions :                                               */
/******************************************************************************/

int main(void)
  {
/******************************************************************************/

  uint16_t startIndex    = NUMERICAL_WRAPPING_ENDPOINT + 1,
           endIndex      = NUMERICAL_WRAPPING_ENDPOINT + 1,
           midPoint      = 0,
           upperDistance = 0,
           lowerDistance = 0,
           interDistance = 0,
           midDistance   = 0;

/******************************************************************************/

  system("cls");

  printf("\n NUMERICAL WRAPPING SIMULATION : ");
  printf("\n ----------------------------- ");

  printf("\n");

  while (startIndex == endIndex)
    {
    startIndex = NUMERICAL_WRAPPING_ENDPOINT;
    endIndex   = NUMERICAL_WRAPPING_ENDPOINT;

    while (startIndex > (NUMERICAL_WRAPPING_ENDPOINT - 1))
      {
      printf("\n Enter an unsigned start index 0 { ... } %u : ", (NUMERICAL_WRAPPING_ENDPOINT - 1));
      scanf("%hu", &startIndex);
      }
   
    while (endIndex > (NUMERICAL_WRAPPING_ENDPOINT - 1))
      {
      printf("\n Enter an unsigned end index 0 { ... } %u : ", (NUMERICAL_WRAPPING_ENDPOINT - 1));
      scanf("%hu", &endIndex);
      }
   
    printf("\n StartIndex = %hu EndIndex = %hu", startIndex, endIndex);
    }

  if (endIndex > startIndex)
    {
    // <Start> precedes <End> in the interval - the mid-point is clearly defined
    midPoint = startIndex + ((endIndex - startIndex) >> 1);
    }
  else
    {
    upperDistance = (NUMERICAL_WRAPPING_ENDPOINT - 1) - startIndex;
    lowerDistance = endIndex;

    interDistance = upperDistance + lowerDistance;
    midDistance   = interDistance >> 1;

    if ((startIndex + midDistance) >= NUMERICAL_WRAPPING_ENDPOINT)
      {
      midPoint = endIndex - midDistance;
      }
    else
      {
      midPoint = startIndex + midDistance;
      }
    }

  printf("\n MidPoint = %hu UpperDistabce = %hu LowerDistance = %hu interDistance = %hu MidDistance = %hu", midPoint, upperDistance, lowerDistance, interDistance, midDistance);

  printf("\n\n");

  while (!_kbhit())
    ;

/******************************************************************************/

  return(0);

/******************************************************************************/
  } /* end of main                                                            */

/******************************************************************************/
