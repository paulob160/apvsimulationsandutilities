/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* mt19937.h                                                                  */
/* 20.09.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/

#ifndef _MT19937_H_
#define _MT19937_H_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

/******************************************************************************/
/* Constant Definitions :                                                     */
/******************************************************************************/

/* Period parameters */  
#define N 624
#define M 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

/******************************************************************************/
/* Function Declarations :                                                    */
/******************************************************************************/

extern void          init_genrand(unsigned long s);
extern void          init_by_array(unsigned long init_key[], int key_length);
extern unsigned long genrand_int32(void);
extern long          genrand_int31(void);
extern double        genrand_real1(void);
extern double        genrand_real2(void);
extern double        genrand_real3(void);
extern double        genrand_res53(void) ;

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/