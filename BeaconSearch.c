/******************************************************************************/
/*                                                                            */
/* BeaconSearch.c                                                             */
/* 15.10.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/* - the inner and outer functions of this program search for a single point  */
/*   (representing a beacon signal) in a search window of 0 { ... } (N - 1)   */
/*   where 'N' is a binary number. Using a non-recursive stateful search the  */
/*   functions locate the point/'signal' to single bit precision i.e. exactly */
/*                                                                            */
/******************************************************************************/
/* Includes :                                                                 */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <conio.h>
#include <time.h>
#include "mt19937.h"

/******************************************************************************/
/* Local Constants :                                                          */
/******************************************************************************/

#define BEACON_SEARCH_LENGTH                (1024)
#define BEACON_SEARCH_START                 (0)
                                            
#define ARGV_0                              (0)
#define ARGV_1                              (ARGV_0 + 1)
                                            
#define BEACON_SEARCH                       ARGV_0 // program name
#define BEACON_SEARCH_WINDOW_LENGTH         ARGV_1 // beacon transmit index
#define BEACON_SEARCH_ARGUMENTS             (ARGV_1 + 1)

#define MINIMUM_BEACON_SEARCH_WINDOW_LENGTH   (16)
#define MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH (2048)

#define WORD_LEADING_BIT_MASK               ((uint16_t)0x8000)
#define WORD_MAXIMUM_LEADING_BIT_SHIFT      (15)

#define MAXIMUM_PRESS_INDEX                 (16)
#define BEACON_SEARCH_EXIT                  'x'

/******************************************************************************/
/* Local Type Definitions :                                                   */
/******************************************************************************/

typedef enum searchLevel_tTag 
  {
  SEARCH_LEVEL_TOP = 0,
  SEARCH_LEVEL_LEFT,
  SEARCH_LEVEL_RIGHT,
  SEARCH_LEVEL_EMPTY,
  SEARCH_LEVEL_FINISHED,
  SEARCH_LEVEL_DONE,
  SEARCH_LEVELS
  } searchLevel_t;

typedef struct searchLimits_tTag
  {
  searchLevel_t searchLevel;
  uint16_t      lowerLimit;
  uint16_t      upperLimit;
  } searchLimits_t;

typedef struct beaconSearchLimits_tTag
  {
  searchLimits_t leftLimit;
  searchLimits_t rightLimit;
  } beaconSearchLimits_t;

typedef enum boolean_tTag
  {
  FALSE = 0,
  TRUE
  } boolean_t;

/******************************************************************************/
/* Local Variable Definitions :                                               */
/******************************************************************************/

uint16_t beaconSearchPosition      = 0,
         beaconSearchWindowLengthT = 0,
         beaconSearchWindowLength  = MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH + 1,
         pressIndex                = 0;

int      press[MAXIMUM_PRESS_INDEX];

/******************************************************************************/
/* Local Function Declarations :                                              */
/******************************************************************************/

int       main(int argc, char *argv[]);
boolean_t computeBeaconSearchLimits(searchLimits_t        limits,
	                                beaconSearchLimits_t *newSearchLimits,
	                                void(*searchProcedure)(void *searchResult));
void      getBeaconLocation(void *beaconLocation);

/******************************************************************************/
/* Local Function Definitions :                                               */
/******************************************************************************/

int main(int argc, char *argv[])
  {
/******************************************************************************/

  uint16_t             leadingBit         = 0,
                       searchDepth        = 0,
                       maximumSearchDepth = 0;

  beaconSearchLimits_t binarySearchLimits = {
                                              { SEARCH_LEVEL_TOP, 0, 0 },
                                              { SEARCH_LEVEL_TOP, 0, 0 }
                                            };

  searchLimits_t       searchLimits       = { 0, 0 };

  boolean_t            searchInProgress   = FALSE,
                       searchSuccess      = FALSE;

  __time64_t           systemTime         = 0;

/******************************************************************************/

  system("cls");
  printf("\n BEACON SEARCH : ");
  printf("\n ---------------");

  /******************************************************************************/
  /* Initialise the Mersenne Twister random-number generator                    */
  /******************************************************************************/

  _time64( &systemTime );

  init_genrand64(systemTime);

  /******************************************************************************/

  if (argc != BEACON_SEARCH_ARGUMENTS)
    {
	  printf("\n BeaconSearch : %d <= <beacon_search_window_length> <= %d", MINIMUM_BEACON_SEARCH_WINDOW_LENGTH, MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH);
	  printf("\n");
	  exit(0);
    }
  else
    {
    /******************************************************************************/
    /* Ensure the beacon search window length is a "round" binary number          */
    /******************************************************************************/

	  beaconSearchWindowLength  = (uint16_t)atoi(argv[BEACON_SEARCH_WINDOW_LENGTH]);

    if (beaconSearchWindowLength < MINIMUM_BEACON_SEARCH_WINDOW_LENGTH)
      {
	    printf("\n BeaconSearch : %d <= <beacon_search_window_length> <= %d", MINIMUM_BEACON_SEARCH_WINDOW_LENGTH, MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH);
	    printf("\n");
	    exit(0);
      }

    beaconSearchWindowLengthT = beaconSearchWindowLength;
    leadingBit                = 0;

    while (( beaconSearchWindowLengthT & WORD_LEADING_BIT_MASK ) == 0)
      {
      leadingBit                = leadingBit + 1;
      beaconSearchWindowLengthT = beaconSearchWindowLengthT << 1;
      }

    beaconSearchWindowLength = 1 << (WORD_MAXIMUM_LEADING_BIT_SHIFT - leadingBit);

    if (beaconSearchWindowLength > MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH)
      {
      beaconSearchWindowLength = MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH;
      }

    beaconSearchPosition       = (uint16_t)(genrand64_real1() * ((double)MAXIMUM_BEACON_SEARCH_WINDOW_LENGTH));

    printf("\n Beacon Position = %d Beacon Window Search Width = %d\n", beaconSearchPosition, beaconSearchWindowLength);

	  if (beaconSearchPosition >= beaconSearchWindowLength)
	    {
	    printf("\n BEACON INDEX OUT-OF-BOUNDS : NO BEACON WILL BE FOUND!\n");
	    }

      for (beaconSearchPosition = 0; beaconSearchPosition < beaconSearchWindowLength; beaconSearchPosition++)
        {
        printf("\n SEARCHING FOR A BEACON AT : %d\n", beaconSearchPosition);

        /******************************************************************************/
        /* Initialise the search into two equal halves                                */
        /******************************************************************************/

        binarySearchLimits.leftLimit.searchLevel = SEARCH_LEVEL_LEFT;
        binarySearchLimits.leftLimit.lowerLimit  = 0;
        binarySearchLimits.leftLimit.upperLimit  = (beaconSearchWindowLength >> 1) - 1;
  
        binarySearchLimits.rightLimit.searchLevel = SEARCH_LEVEL_RIGHT;
        binarySearchLimits.rightLimit.lowerLimit  = (beaconSearchWindowLength >> 1);
        binarySearchLimits.rightLimit.upperLimit  = beaconSearchWindowLength - 1;
  
        /******************************************************************************/
        /* Start with the left-hand search                                            */
        /******************************************************************************/
  
        searchLimits.searchLevel = binarySearchLimits.leftLimit.searchLevel;
        searchLimits.lowerLimit  = binarySearchLimits.leftLimit.lowerLimit;
        searchLimits.upperLimit  = binarySearchLimits.leftLimit.upperLimit;
  
        printf("\n **************************************");
        printf("\n LEFT SEGMENT : SEARCHING FOR BEACON AT  [ %3d - %3d ]", searchLimits.lowerLimit, searchLimits.upperLimit);
        printf("\n **************************************\n");

        /******************************************************************************/
        /* Call the searching function and resolve left, right, next-depth or failure */
        /******************************************************************************/
  
        searchInProgress   = TRUE;
        searchDepth        = 0;

        while (searchInProgress == TRUE)
          {
          if (searchDepth > maximumSearchDepth)
            {
            maximumSearchDepth = searchDepth;

            }

          printf("\n         SEARCH DEPTH = [ %02d ]\n", searchDepth);
          printf("\n MAXIMUM SEARCH DEPTH = [ %02d ]\n", maximumSearchDepth);

          searchSuccess = computeBeaconSearchLimits( searchLimits,
                                                    &binarySearchLimits,
                                                     getBeaconLocation);
      
          if (( binarySearchLimits.leftLimit.searchLevel  == SEARCH_LEVEL_DONE ) &&
              ( binarySearchLimits.rightLimit.searchLevel == SEARCH_LEVEL_DONE ))
            {
            if (searchSuccess == TRUE)
              {
              /******************************************************************************/
              /* Success!!!                                                                 */
              /******************************************************************************/
  
              printf("\n **************************************");
              printf("\n BEACON FOUND AT %d - %d", searchLimits.lowerLimit, searchLimits.upperLimit);
              printf("\n **************************************\n");

              searchInProgress = FALSE;
  
              /******************************************************************************/
              }
            else
              {
              /******************************************************************************/
              /* A new left/right segment search has been signalled                         */
              /******************************************************************************/
      
              searchDepth = searchDepth + 1;

              binarySearchLimits.leftLimit.searchLevel  = SEARCH_LEVEL_LEFT;
              binarySearchLimits.rightLimit.searchLevel = SEARCH_LEVEL_RIGHT;
      
              searchLimits.searchLevel = binarySearchLimits.leftLimit.searchLevel;
  
              searchLimits.lowerLimit  = binarySearchLimits.leftLimit.lowerLimit;
              searchLimits.upperLimit  = binarySearchLimits.leftLimit.upperLimit;
  
              printf("\n **************************************");
              printf("\n LEFT SEGMENT : SEARCHING FOR BEACON AT  [ %3d - %3d ]", searchLimits.lowerLimit, searchLimits.upperLimit);
              printf("\n **************************************\n");

              /******************************************************************************/
              }
            }
          else
            {
            if (( binarySearchLimits.leftLimit.searchLevel  == SEARCH_LEVEL_RIGHT ) &&
                ( binarySearchLimits.rightLimit.searchLevel == SEARCH_LEVEL_RIGHT ))
              {
              /******************************************************************************/
              /* Search the right-hand segment                                              */
              /******************************************************************************/
  
              searchDepth = searchDepth + 1;

              searchLimits.searchLevel = binarySearchLimits.rightLimit.searchLevel;
  
              searchLimits.lowerLimit = binarySearchLimits.rightLimit.lowerLimit;
              searchLimits.upperLimit = binarySearchLimits.rightLimit.upperLimit;
  
              printf("\n **************************************");
              printf("\n RIGHT SEGMENT : SEARCHING FOR BEACON AT [ %3d - %3d ]", searchLimits.lowerLimit, searchLimits.upperLimit);
              printf("\n **************************************\n");

              /******************************************************************************/
              }
            else
              {
              if (( binarySearchLimits.leftLimit.searchLevel == SEARCH_LEVEL_EMPTY ) &&
                  ( binarySearchLimits.rightLimit.searchLevel == SEARCH_LEVEL_EMPTY ))
                {
                /******************************************************************************/
                /* The search has failed!!!                                                   */
                /******************************************************************************/
  
                //printf("\n BEACON NOT FOUND!\n");
                searchInProgress = FALSE;
  
                /******************************************************************************/
                }
              }
            }
          }

	      printf("\n");

        while (!_kbhit())
          {
          }

        pressIndex = 0;

        while (_kbhit())
          {
          press[pressIndex] = _getch();
          pressIndex        = pressIndex + 1;
          }

        for (pressIndex = 0; pressIndex < MAXIMUM_PRESS_INDEX; pressIndex++)
          { 
          if (press[pressIndex] == BEACON_SEARCH_EXIT)
            {
            exit(0);
            }
          }
        }
      }

      /******************************************************************************/

      while (press[0] != BEACON_SEARCH_EXIT)
        {
        if (_kbhit())
          {
          press[0] = _getch();

          if (press[0] == BEACON_SEARCH_EXIT)
            {
            exit(0);
            }
          }
        }


/******************************************************************************/
  } /* end of main                                                            */

/******************************************************************************/
/* computeBeaconSearchLimits() :                                              */
/*   --> limits          : the bounding values for the search segm            */
/*   <-- newSearchLimits : the result of a segment search and the new segment */
/*                         boundaries if any                                  */
/*   --> searchProcedure : the beacon detection algorithm                     */
/*                                                                            */
/* - this function attempts to find a beacon transmission by a non-recursive  */
/*   left- and right-search of a bounded segment. The function returns a new  */
/*   set of segment boundaries if the beacon is found ambiguously or          */
/*   "success" if it is found uniquely. Otherwise "failure" is returned.      */
/*                                                                            */
/******************************************************************************/

boolean_t computeBeaconSearchLimits(searchLimits_t        limits,
	                                  beaconSearchLimits_t *newSearchLimits,
	                                  void                 (*searchProcedure)(void *searchResult))
  {
/*****************************************************************************/

         uint16_t      beaconLocation = 0,
                       limitDistance  = 0;

         boolean_t     beaconFound    = FALSE;

  static searchLevel_t searchLevel    = SEARCH_LEVEL_TOP;

/******************************************************************************/
  
  searchProcedure((void *)&beaconLocation);

  /******************************************************************************/
  /* Start searching on the left                                                */
  /******************************************************************************/

  if (limits.searchLevel == SEARCH_LEVEL_LEFT)
    {
    if (limits.lowerLimit == 0)
      {
      if (beaconLocation <= limits.upperLimit)
        {
        /******************************************************************************/
        /* If the beacon has been found, signal "search over"                         */
        /******************************************************************************/

        //if (((abs( limits.upperLimit - limits.lowerLimit ) == 1)  || ( limits.upperLimit - limits.lowerLimit ) == 0))
        if (( limits.upperLimit - limits.lowerLimit ) == 0)
          {
          newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_DONE;
          newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_DONE;
          beaconFound                             = TRUE;
          }
        else
          {
          /******************************************************************************/
          /* The beacon has been found in this segment, signal and compute a new search */
          /* depth                                                                      */
          /******************************************************************************/

          newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_DONE;
          newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_DONE;

          newSearchLimits->leftLimit.lowerLimit = 0;
          newSearchLimits->leftLimit.upperLimit = ((limits.upperLimit + 1) >> 1) - 1;

          newSearchLimits->rightLimit.upperLimit = limits.upperLimit;
          newSearchLimits->rightLimit.lowerLimit  = ((limits.upperLimit + 1) >> 1);

          /******************************************************************************/
          }
        }
      else
        {
        /******************************************************************************/
        /* The beacon has NOT been found in this segment, search the right-hand       */
        /* segment next                                                               */
        /******************************************************************************/

        newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_RIGHT;

        /******************************************************************************/
        }
      }
    else
      {
      if (( beaconLocation >= limits.lowerLimit ) && ( beaconLocation <= limits.upperLimit ))
        {
        //if ((abs( limits.upperLimit - limits.lowerLimit ) == 1) || ( limits.upperLimit - limits.lowerLimit ) == 0)
        if (( limits.upperLimit - limits.lowerLimit ) == 0)
          {
          /******************************************************************************/
          /* If the beacon has been found, signal "search over"                         */
          /******************************************************************************/

          newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_DONE;
          newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_DONE;
          beaconFound                             = TRUE;
          }
        else
          {
          /******************************************************************************/
          /* The beacon has been found in this segment, signal and compute a new search */
          /* depth                                                                      */
          /******************************************************************************/

          newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_DONE;
          newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_DONE;

          limitDistance                           = limits.upperLimit - limits.lowerLimit;

          newSearchLimits->leftLimit.lowerLimit   = limits.lowerLimit;
          newSearchLimits->leftLimit.upperLimit   = limits.lowerLimit + (limitDistance >> 1);

          newSearchLimits->rightLimit.upperLimit  = limits.upperLimit;
          newSearchLimits->rightLimit.lowerLimit  = limits.lowerLimit + ((limitDistance + 1) >> 1);

          /******************************************************************************/
          }
        }
      else
        {
        /******************************************************************************/
        /* The beacon has NOT been found in the left-hand segment, search the right-  */
        /* hand segment next                                                          */
        /******************************************************************************/

        newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_RIGHT;

        /******************************************************************************/
        }
      }
    }
  else
    {
    /******************************************************************************/
    /* This is a right-hand segment search                                        */
    /******************************************************************************/

      if (( beaconLocation >= limits.lowerLimit ) && ( beaconLocation <= limits.upperLimit ))
        {
        //if (((abs( limits.upperLimit - limits.lowerLimit ) == 1) || ( limits.upperLimit - limits.lowerLimit ) == 0))
        if (( limits.upperLimit - limits.lowerLimit ) == 0)
          {
          /******************************************************************************/
          /* If the beacon has been found, signal "search over"                         */
          /******************************************************************************/

          newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_DONE;
          newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_DONE;
          beaconFound                             = TRUE;
          }
        else
          {
          /******************************************************************************/
          /* The beacon has been found in this segment, signal and compute a new search */
          /* depth                                                                      */
          /******************************************************************************/

          newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_DONE;
          newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_DONE;

          limitDistance                           = limits.upperLimit - limits.lowerLimit;

          newSearchLimits->leftLimit.lowerLimit   = limits.lowerLimit;
          newSearchLimits->leftLimit.upperLimit   = limits.lowerLimit + ((limitDistance + 1) >> 1);

          newSearchLimits->rightLimit.upperLimit  = limits.upperLimit;
          newSearchLimits->rightLimit.lowerLimit  = limits.lowerLimit + (limitDistance >> 1);

          /******************************************************************************/
          }
        }
      else
        {
        /******************************************************************************/
        /* The beacon has NOT been found in the right-hand segment so the search has  */
        /* failed!!!                                                                  */
        /******************************************************************************/

        newSearchLimits->leftLimit.searchLevel  = SEARCH_LEVEL_EMPTY;
        newSearchLimits->rightLimit.searchLevel = SEARCH_LEVEL_EMPTY;

        /******************************************************************************/
        }

    /******************************************************************************/
    }

/******************************************************************************/

  return(beaconFound);

/******************************************************************************/
  } /* end of computeBeaconSearchLimits                                       */

/******************************************************************************/
/* getBeaconLocation :                                                        */
/*  <--> beaconLocation : index of the beacon location                        */
/*                                                                            */
/* - proxy for a mechanism for detecting a beacon                             */
/*                                                                            */
/******************************************************************************/

void getBeaconLocation(void *beaconLocation)
  {
/******************************************************************************/

  *(uint16_t *)beaconLocation = beaconSearchPosition;

/******************************************************************************/
  } /* end of getBeaconLocation                                                 */
  
/******************************************************************************/

