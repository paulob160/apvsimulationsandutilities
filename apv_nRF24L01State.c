/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* apv_nRF24L01State.c                                                        */
/* 21.09.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "ApvError.h"
#include "apv_NRF24L01State.h"
#include "stdbool.h"

/******************************************************************************/
/* Variable Definitions :                                                     */
/******************************************************************************/

uint16_t apv_nRF24L01BeaconTimingPoint     = 0,
         apv_nRF24L01BeaconFullPeriodStart = 0,
         apv_nRF24L01BeaconFullPeriodEnd   = APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD;

boolean_t apv_nRF24L01InitialiseSearch     = TRUE;

boolean_t apv_nRF24L01BeaconFound          = FALSE;
uint16_t  apv_nRF24L01BeaconTiming         = 0;

/******************************************************************************/
/* Function Definitions :                                                     */
/******************************************************************************/
/* apv_nRF24L01MasterSlaveBeaconSynchronisation() :                           */
/*  --> fullPeriodStart      : the index representing the start of the full   */
/*                             transmit/receive cycle                         */
/*  --> fullPeriodEnd        :  the index representing the end of the full    */
/*                             transmit/receive cycle                         */
/*  --> initialiseSearch     : force the search to restart :                  */
/*                                 [ FALSE == no restart | TRUE == restart ]  */
/*                                                                            */
/* - this is a stateful function called per time segment of the search space. */
/*   The intention is to find the start and end-timing points where the radio */
/*   receiver should be switched on and switched off. This defines a window   */
/*   in which the master beacon signal is expected to arrive. The window time */
/*   period is successively reduced in width until the remaining space should */
/*   contain the beacon. The search is a binary chopping algorithm.           */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01MasterSlaveBeaconSynchronisation(uint16_t   fullPeriodStart,
                                                            uint16_t   fullPeriodEnd,
                                                            boolean_t  initialiseSearch,
                                                            boolean_t *beaconFound,
                                                            uint16_t  *beaconTiming)
  {
/******************************************************************************/

  static uint16_t                      searchWindowStart     = 0,
                                       searchWindowEnd       = 0,
                                       searchWindowLowStart  = 0,
                                       searchWindowLowEnd    = 0,
                                       searchWindowHighStart = 0,
                                       searchWindowHighEnd   = 0;

  static apv_nRF24L01SearchDirection_t searchDirection       = APV_NRF24L01_SEARCH_DIRECTION_FULL; // defines how the binary search starts

         boolean_t                     searchLocationFound   = FALSE;

         APV_ERROR_CODE                synchronisationError  = APV_ERROR_CODE_NONE;

/******************************************************************************/

  if (initialiseSearch == TRUE)
    {
    // The initial search window is the whole of the period but TERMINATES at the wraparound i.e. in an interval of 1000msecs 
    // the search starts at t = 0 and stops at t = 0
    searchWindowStart = fullPeriodStart;
    searchWindowEnd   = (fullPeriodStart + APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD) % APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD;

    searchDirection = APV_NRF24L01_SEARCH_DIRECTION_FULL;
    }

  // This function represents the passage of 'n' milliseconds between opening and closing the receive window
  synchronisationError = apv_nRF24L01SimulationBeaconSearch( searchWindowStart,
                                                             searchWindowEnd,
                                                             fullPeriodStart,
                                                             fullPeriodEnd,
                                                             apv_nRF24L01BeaconTimingPoint, // global beacon timing
                                                            &searchLocationFound);

  // Has the beacon found found in this search segment ?
  if (searchLocationFound == TRUE)
    {
    // Subdivide the segment into two new ones, "low" and "high"
    if ((searchDirection == APV_NRF24L01_SEARCH_DIRECTION_FULL) || (searchDirection = APV_NRF24L01_SEARCH_DIRECTION_LOWER))
      {
      apv_nRF24L01ComputeAndHalveInterval( searchWindowStart,
                                           searchWindowEnd,
                                          &searchWindowLowStart,
                                          &searchWindowLowEnd,
                                          &searchWindowHighStart,
                                          &searchWindowHighEnd);

      // At the next search examine the "upper" segment
      searchWindowStart = searchWindowHighStart;
      searchWindowEnd   = searchWindowHighEnd;

      searchDirection = APV_NRF24L01_SEARCH_DIRECTION_UPPER;
      }
    else
      {
      // At the next search examine the "lower" segment
      searchWindowStart = searchWindowLowStart;
      searchWindowEnd   = searchWindowLowEnd;

      searchDirection = APV_NRF24L01_SEARCH_DIRECTION_LOWER;
      }
    }

/******************************************************************************/

  return(synchronisationError);

/******************************************************************************/
  } /* end of apv_nRF24L01MasterSlaveBeaconSynchronisation                    */

/******************************************************************************/
/* apv_nRF24L01ComputeAndHalveInterval() :                                    */
/*                                                                            */
/* - compute the start and end locations of two sub-segments of a larger one. */
/*   Ensure the calling search algorithms can work from the lower index to    */
/*   higher index of the start and end location indices                       */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01ComputeAndHalveInterval(uint16_t  intervalStart,
                                                   uint16_t  intervalEnd,
                                                   uint16_t *lowerSearchIntervalStart,
                                                   uint16_t *lowerSearchIntervalEnd,
                                                   uint16_t *upperSearchIntervalStart,
                                                   uint16_t *upperSearchIntervalEnd)
  {
/******************************************************************************/

  uint16_t       interval     = 0;

  APV_ERROR_CODE computeError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  // The interval needs to be wide enough to divide
  if (intervalStart < intervalEnd)
    {
    interval = intervalEnd - intervalStart;
    }
  else
    {
    interval = (((uint16_t)APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD) - intervalStart) + intervalEnd;
    }

  if (interval < ((uint16_t)APV_NRF24L01_BEACON_SYNCHRONISATION_MINIMUM))
    {
    computeError = APV_ERROR_CODE_PARAMETER_OUT_OF_RANGE;
    }

/******************************************************************************/

  return(computeError);

/******************************************************************************/
  } /* end of apv_nRF24L01ComputeAndHalveInterval                             */

/******************************************************************************/
/* SIMULATION SUPPORT CODE :                                                  */
/******************************************************************************/
/* apv_nRF24L01SimulationBeaconSearch() :                                     */
/*  --> searchWindowStart    : timing marker for opening the receiver for the */
/*                             beacon search                                  */
/*  --> searchWindowEnd      : timing marker for closing the receiver for the */
/*                             beacon search                                  */
/*  --> fullPeriodStart      : the index representing the start of the full   */
/*                             transmit/receive cycle                         */
/*  --> fullPeriodEnd        :  the index representing the end of the full    */
/*                             transmit/receive cycle                         */
/*  --> searchBeaconLocation : the tiing marker for the beacon trsansmission  */
/*  <-- searchError          : error codes                                    */
/*                                                                            */
/* - for the simulation the prescence of the master beacon transmission is    */
/*   represented by a timing marker. This function searches for the marker.   */
/*   The beacon is a matching code. If the beacon has been received in the    */
/*   search window the receive will match it's copy of the beacon pattern to  */
/*   detect it (this is not a fine-timing match! timings here are +/-1 msec). */
/*   The search variables are converted to signed integers for convenience.   */
/*   Currently the full period is 0 - 999 msec at 1 msec intervals, easily    */
/*   contained in an "int16_t". If the period ever exceeds +/-32767...worry   */
/*   about it then!                                                           */
/*                                                                            */
/******************************************************************************/

APV_ERROR_CODE apv_nRF24L01SimulationBeaconSearch(uint16_t   searchWindowStart,
                                                  uint16_t   searchWindowEnd,
                                                  uint16_t   fullPeriodStart,
                                                  uint16_t   fullPeriodEnd,
                                                  uint16_t   searchBeaconLocation,
                                                  boolean_t *searchBeaconLocationFound)
  {
/******************************************************************************/

  APV_ERROR_CODE searchError = APV_ERROR_CODE_NONE;

/******************************************************************************/

  int16_t searchWindowStart_s    = searchWindowStart,
          searchWindowEnd_s      = searchWindowEnd,
          fullPeriodStart_s      = fullPeriodStart,
          fullPeriodEnd_s        = fullPeriodEnd,
          searchBeaconLocation_s = searchBeaconLocation;

  int16_t searchWindowIndex      = 0;

/******************************************************************************/

  // Assume no detection
  *searchBeaconLocationFound = FALSE;

  // Start searching low-to-high
  searchWindowIndex = searchWindowStart_s;

  do
    {
    if (searchWindowIndex == searchBeaconLocation)
      {
      *searchBeaconLocationFound = TRUE;
       break;
      }

    // Wrap-around if the search index runs off the end of the full period
    if (searchWindowIndex > fullPeriodEnd)
      {
      searchWindowIndex = fullPeriodStart;
      }
    else
      {
      searchWindowIndex = searchWindowIndex + 1;
      }
    }
  while (searchWindowIndex != searchWindowEnd_s);

/******************************************************************************/
  } /* end of apv_nRF24L01SimulationBeaconSearch                              */

/******************************************************************************/
/* apv_nRF24L01BeaconDetection() :                                            */
/*  <-- beaconDetected      : [ FALSE == not detected | TRUE == detected ]    */
/*  <-- beaconTimingPoint   : 0 { <timing-point> } 999 (msecs)                */
/*  --> inhibitTimingChange : the first timing point generated is preserved   */
/*                                                                            */
/* - abstracts the beacon detector. The receive pattern-matching is replaced  */
/*   by (i) randomly-generating the beacon trsnsmission (ii) randomly-        */
/*   generating the beacon timing marker. A new beacon timing is generated IF */
/*   no/lost previous beacon timing exists                                    */
/*                                                                            */
/******************************************************************************/

#ifdef WIN32
#include "mt19937.h"
#endif

APV_ERROR_CODE apv_nRF24L01BeaconDetection(boolean_t  *beaconDetected,
                                           uint16_t   *beaconTimingPoint,
                                           boolean_t   inhibitTimingChange)
  {
/******************************************************************************/

  static boolean_t      inhibitTimingChangeLock = FALSE; // for test the first beacon timing can be kept present

         APV_ERROR_CODE detectionError          = APV_ERROR_CODE_NONE;

/******************************************************************************/

#ifdef WIN32
  // There is a chance the beacon is NOT present
  uint8_t beaconTime   = 0;
  double  randomTiming = genrand_real1();

  // Limit the random number range
  randomTiming = randomTiming * ((double)APV_NRF24L01_RANDOM_BEACON_NOT_PRESENT_LIMIT);
  beaconTime   = (uint8_t)randomTiming;

  // Detect the chance of "beacon not present"
  if (beaconTime >= ((uint8_t)APV_NRF24L01_RANDOM_BEACON_NOT_PRESENT))
    {
    if (inhibitTimingChangeLock == FALSE)
      {
      *beaconDetected = FALSE;
      }
    }
  else
    {
    // Generate the beacon timing IF no earlier beacon was found
    if (*beaconDetected == FALSE)
      {
      if ((inhibitTimingChange == TRUE) && (inhibitTimingChangeLock == FALSE))
        {
         randomTiming            = genrand_real1();
        *beaconTimingPoint       = (uint16_t)(randomTiming * ((double)(APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD - 1)));
        *beaconDetected          = TRUE;

         inhibitTimingChangeLock = TRUE;
        }
      else
        {
         randomTiming            = genrand_real1();
        *beaconTimingPoint       = (uint16_t)(randomTiming * ((double)(APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD - 1)));
        *beaconDetected          = TRUE;
        }
      }
    }

#endif

/******************************************************************************/

  return(detectionError);

/******************************************************************************/
  } /* end of apv_nRF24L01BeaconDetection                                     */

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/