/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* BeaconSynchronisation.h                                                    */
/* 20.09.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/

#ifndef _BEACON_SYNCHRONISATION_H_
#define _BEACON_SYNCHRONISATION_H_

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "stdbool.h"

/******************************************************************************/
/* Constant Definitions :                                                     */
/******************************************************************************/

#define SIMULATION_PARAMETER_NAME_MAXIMUM_LENGTH   (64)

#define MASTER_TRANSMISSION_TIMING_POINT         "MASTER_TRANSMISSION_TIMING_POINT"
#define SLAVE_RECEPTION_TIMING_POINT             "SLAVE_RECEPTION_TIMING_POINT"

#define SEARCH_BOUNDARY_WRAPAROUND               ((uint16_t)0xffff) // to do the two's-complement!

#define BEACON_EXIT                              'b' // keypress to exit the beacon test loop

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/

typedef enum simulationParameterType_tTag
  {
  SIMULATION_PARAMETER_TYPE_UINT8 = 0,
  SIMULATION_PARAMETER_TYPE_UINT16,
  SIMULATION_PARAMETER_TYPE_UINT32,
  SIMULATION_PARAMETER_TYPE_UINT64,
  SIMULATION_PARAMETER_TYPE_INT8,
  SIMULATION_PARAMETER_TYPE_INT16,
  SIMULATION_PARAMETER_TYPE_INT32,
  SIMULATION_PARAMETER_TYPE_INT64,
  SIMULATION_PARAMETER_TYPE_UCHAR,
  SIMULATION_PARAMETER_TYPE_USHORT,
  SIMULATION_PARAMETER_TYPE_UINT,
  SIMULATION_PARAMETER_TYPE_ULONG,
  SIMULATION_PARAMETER_TYPE_ULONGLONG,
  SIMULATION_PARAMETER_TYPE_CHAR,
  SIMULATION_PARAMETER_TYPE_SHORT,
  SIMULATION_PARAMETER_TYPE_INT,
  SIMULATION_PARAMETER_TYPE_LONG,
  SIMULATION_PARAMETER_TYPE_LONGLONG,
  SIMULATION_PARAMETER_TYPE_FLOAT,
  SIMULATION_PARAMETER_TYPE_DOUBLE,
  SIMULATION_PARAMETER_TYPE_LONGDOUBLE,
  SIMULATION_PARAMETER_TYPE_STRING,
  SIMULATION_PARAMETER_TYPES
  } simulationParameterType_t;

typedef struct simulationParameter_tTag
  {
  char                       simulationParameterName[SIMULATION_PARAMETER_NAME_MAXIMUM_LENGTH];
  void                      *simulationParameter;
  simulationParameterType_t  simulationParameterType;
  } simulationParameter_t;

/******************************************************************************/
/* Function Declarations :                                                    */
/******************************************************************************/

extern void printSimulationParameter(simulationParameter_t *simulationParameter,
                                     boolean_t              printSimulationParameterNewLine);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/