/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/
/*                                                                            */
/* apv_nRF24L01State.h                                                        */
/* 21.09.18                                                                   */
/* Paul O'Brien                                                               */
/*                                                                            */
/******************************************************************************/

#ifndef _APV_NRF24L01_STATE_H_
#define _APV_NRF24L01_STATE_H_

/******************************************************************************/
/* Include Files :                                                            */
/******************************************************************************/

#include <stdint.h>
#include "ApvError.h"
#include "stdbool.h"

/******************************************************************************/
/* Constant Definitions :                                                     */
/******************************************************************************/

#ifdef WIN32
#define APV_NRF24L01_RANDOM_BEACON_NOT_PRESENT_LIMIT (100) // set the "beacon not present" range
#define APV_NRF24L01_RANDOM_BEACON_NOT_PRESENT        (90) // 1 in 10 chance of "beacon not present"
#endif

#define APV_NRF24L01_BEACON_SYNCHRONISATION_PERIOD  (1000) // the operations sequencer splits each second into 1000 milliseconds
#define APV_NRF24L01_BEACON_SYNCHRONISATION_MINIMUM    (2) // the minimum span of a search interval

/******************************************************************************/
/* Type Definitions :                                                         */
/******************************************************************************/

typedef enum apv_nRF24L01SearchDirection_tTag
  {
  APV_NRF24L01_SEARCH_DIRECTION_FULL = 0,
  APV_NRF24L01_SEARCH_DIRECTION_UPPER,
  APV_NRF24L01_SEARCH_DIRECTION_LOWER,
  APV_NRF24L01_SEARCH_DIRECTIONS
  } apv_nRF24L01SearchDirection_t;

/******************************************************************************/
/* Variable Declarations :                                                    */
/******************************************************************************/

extern uint16_t  apv_nRF24L01BeaconTimingPoint;
extern uint16_t  apv_nRF24L01BeaconFullPeriodStart;
extern uint16_t  apv_nRF24L01BeaconFullPeriodEnd;
extern boolean_t apv_nRF24L01InitialiseSearch;
extern boolean_t apv_nRF24L01BeaconFound;
extern uint16_t  apv_nRF24L01BeaconTiming;

/******************************************************************************/
/* Function Declarations :                                                    */
/******************************************************************************/

extern APV_ERROR_CODE apv_nRF24L01MasterSlaveBeaconSynchronisation(uint16_t   fullPeriodStart,
                                                                   uint16_t   fullPeriodEnd,
                                                                   boolean_t  initialiseSearch,
                                                                   boolean_t *beaconFound,
                                                                   uint16_t  *beaconTiming);
extern APV_ERROR_CODE apv_nRF24L01BeaconDetection(boolean_t *beaconDetected,
                                                  uint16_t  *beaconTimingPoint,
                                                  boolean_t  inhibitTimingChange);
extern APV_ERROR_CODE apv_nRF24L01SimulationBeaconSearch(uint16_t   searchWindowStart,
                                                  uint16_t   searchWindowEnd,
                                                  uint16_t   fullPeriodStart,
                                                  uint16_t   fullPeriodEnd,
                                                  uint16_t   searchBeaconLocation,
                                                  boolean_t *searchBeaconLocationFound);

/******************************************************************************/

#endif

/******************************************************************************/
/* (C) PulsingCoreSoftware Limited 2018 (C)                                   */
/******************************************************************************/